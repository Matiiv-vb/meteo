import { CityItem } from "@/store/interface/city";
export const city: Array<CityItem> = [
  {
    id: 1,
    city: "Ivano-Frankivsk",
    coord: {
      lon: 24.7125,
      lat: 48.9231,
    },
    country: "Ukraine",
    state: "",
    daily: {
      apparent_temperature_max: [],
      apparent_temperature_min: [],
      time: [],
    },
    current_weather: {},
  },
  {
    id: 2,
    city: "Kyiv",
    coord: {
      lon: 30.5238,
      lat: 50.4547,
    },
    country: "Ukraine",
    state: "",
    daily: {
      apparent_temperature_max: [],
      apparent_temperature_min: [],
      time: [],
    },
    current_weather: {},
  },
  {
    id: 3,
    city: "London",
    coord: {
      lon: -0.1257,
      lat: 51.5085,
    },
    state: "",
    country: "United Kingdom",
    daily: {
      apparent_temperature_max: [],
      apparent_temperature_min: [],
      time: [],
    },
    current_weather: {},
  },
  {
    id: 4,
    city: "New York",
    coord: {
      lon: -74.006,
      lat: 40.7143,
    },
    state: "",
    country: "United States",
    daily: {
      apparent_temperature_max: [],
      apparent_temperature_min: [],
      time: [],
    },
    current_weather: {},
  },
  {
    id: 5,
    city: "Paris",
    coord: {
      lon: 2.3488,
      lat: 48.8534,
    },
    state: "",
    country: "France",
    daily: {
      apparent_temperature_max: [],
      apparent_temperature_min: [],
      time: [],
    },
    current_weather: {},
  },
  {
    id: 6,
    city: "Tokyo",
    coord: {
      lon: 139.6917,
      lat: 35.6895,
    },
    state: "",
    country: "Japan",
    daily: {
      apparent_temperature_max: [],
      apparent_temperature_min: [],
      time: [],
    },
    current_weather: {},
  },
  {
    id: 7,
    city: "Chicago",
    coord: {
      lon: -87.65,
      lat: 41.85,
    },
    state: "",
    country: "United States",
    daily: {
      apparent_temperature_max: [],
      apparent_temperature_min: [],
      time: [],
    },
    current_weather: {},
  },
  {
    id: 8,
    city: "Donetsk",
    coord: {
      lon: 37.8022,
      lat: 48.023,
    },
    state: "",
    country: "Ukraine",
    daily: {
      apparent_temperature_max: [],
      apparent_temperature_min: [],
      time: [],
    },
    current_weather: {},
  },
];
