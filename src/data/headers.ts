export const headers: Array<any> = [
  {
    title: "City",
    align: "start",
    sortable: false,
    key: "city",
  },
  {
    title: "Temparature max",
    align: "end",
    key: "daily.apparent_temperature_max[0]",
  },
  {
    title: "Temparature min",
    align: "end",
    key: "daily.apparent_temperature_min[0]",
  },
  {
    title: "Wind direction",
    align: "end",
    key: "current_weather.winddirection",
  },
];
