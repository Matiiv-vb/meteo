// import type { Ref } from 'vue'
import { ref } from "vue";


export function useFilterRange() {
  const maxNumber = ref<number>(50);
  const mixNumber = ref<number>(10);
  
  return {
    mixNumber,
    maxNumber,
  };
}
