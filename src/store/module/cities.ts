import type { CityState } from "../type/city";
import { CityItem } from "../interface/city";
import { defineStore } from "pinia";
import { computed, reactive, ref } from "vue";
import axios from "axios";
import { city } from "@/data/city.ts";

export const useCitiesStore = defineStore("cities", () => {
  const state = reactive<CityState>({
    city: null,
    countries: [],
    cities: [],
  }) as CityState;

  const selectedCountries = ref<string[]>([]);
  const minNumber = ref<number>(-80);
  const maxNumber = ref<number>(80);

  const setMinNumber = (val: number): void => {
    minNumber.value = val;
  };
  const setMaxNumber = (val: number): void => {
    maxNumber.value = val;
  };

  const baseUrl = "https://api.open-meteo.com/v1/forecast?";

  function fetchtCity(lat: number, lon: number): Promise<CityItem | any> {
    const url = new URL(baseUrl);
    const params = new URLSearchParams(url.search);

    params.set("latitude", lat.toString());
    params.set("longitude", lon.toString());
    params.set("daily", "apparent_temperature_max,apparent_temperature_min");
    params.set("current_weather", "true");
    params.set("timezone", "GMT");

    return axios.get<CityItem>(`${baseUrl}${params.toString()}`);
  }

  Promise.all(
    city.map(({ coord }) => {
      return fetchtCity(coord.lat, coord.lon);
    })
  )
    .then((values) => {
      setCities(values);
      setCountries();
      setCity(state.cities[0]);
    })
    .catch((error) => {
      console.log(error);
    });

  const setCountries = (): void => {
    state.countries = [
      ...new Set(
        state.cities.map((el: CityItem) => {
          return el.country;
        })
      ),
    ];
    selectedCountries.value = state.countries;
  };

  const setCities = (value: any): void => {
    state.cities = city.map((el: CityItem, ind: number) => {
      const data = value[ind].data;

      return new CityItem(
        el.id,
        el.city,
        el.coord,
        el.country,
        el.state,
        data.daily,
        data.current_weather
      );
    });
  };

  const setCity = (value: CityItem): void => {
    state.city = value;
  };

  const getCity = computed((): CityItem | null => state.city);
  const getCountries = computed((): string[] => state.countries);

  const getCities = computed((): CityItem[] => {
    return state.cities.filter((el: CityItem) => {
      return (
        selectedCountries.value.includes(el.country) &&
        el?.daily?.apparent_temperature_max[0] <= maxNumber.value &&
        el?.daily?.apparent_temperature_min[0] >= minNumber.value
      );
    });
  });

  return {
    getCity,
    getCities,
    fetchtCity,
    setCities,
    getCountries,
    selectedCountries,
    maxNumber,
    minNumber,
    setMaxNumber,
    setMinNumber,
    setCity,
  };
});
