import type { Coord, Daily } from "../type/city";

export class CityItem {
  constructor(
    public id: number,
    public city: string,
    public coord: Coord,
    public country: string,
    public state: string,
    public daily: Daily,
    public current_weather: any
  ) {}
}
