export function averageArrays(
  arr1: Array<number>,
  arr2: Array<number>
): Array<number> {
  return arr1.map((e: number, ind: number) => {
    return (e + arr2[ind]) / 2;
  });
}

const weekday = [
  "Sun",
  "Mon",
  "Tue",
  "Wed",
  "Thu",
  "Fri",
  "Sat",
];

export function getDays(arr: Array<string>): Array<string> {
  return arr.map((e: string) => {
    const day = new Date(e).getDay();
    return weekday[day];
  });
}
