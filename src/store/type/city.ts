import type { CityItem } from "../interface/city";

export type CityState = {
  city: CityItem | null;
  countries: string[];
  cities: CityItem[];
};

export type Coord = {
  lat: number;
  lon: number;
};

export type Daily = {
  apparent_temperature_max: Array<number>;
  apparent_temperature_min: Array<number>;
  time: Array<string>;
};
