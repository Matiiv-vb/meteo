import { reactive } from "vue";
import { ChartData } from "chart.js";
import { ScriptableContext } from "chart.js";

export const data = reactive<ChartData<"bar", number[]>>({
  labels: ["No data"],
  datasets: [
    {
      label: "Temperature",
      backgroundColor: (context: ScriptableContext<"bar">) => {
        const ctx = context.chart.ctx;
        const gradient = ctx.createLinearGradient(0, 0, 0, 300);
        gradient.addColorStop(0, "#B3FC4F");
        gradient.addColorStop(1, "#173102");
        return gradient;
      },
      data: [0],
    },
  ],
});

export const chartOptions = {
  responsive: true,
  maintainAspectRatio: false,
  borderRadius: 8,
  barPercentage: 0.6,
  scales: {
    x: {
      grid: {
        display: false,
      },
      ticks: {
        color: "#FDFCFF",
        font: {
          size: 18,
        },
      },
    },
    y: {
      border: {
        display: true,
        dash: [12, 8],
      },
      grid: {
        color: "#313131",
        lineWidth: 2,
      },
      ticks: {
        color: "#A4A4A4",
        font: {
          size: 14,
        },
        stepSize: 5,
        beginAtZero: true,
      },
    },
  },
  plugins: {
    title: {
      display: true,
      text: "Analytics",
      align: "start" as const,
      position: "top" as const,
      color: "#ccc",
      font: {
        size: 16,
      },
      padding: {
        top: 0,
        bottom: -10,
      },
    },
    legend: {
      display: true,
      align: "end" as const,
      labels: {
        color: "#E3E3E3",
        font: {
          size: 14,
        },
        usePointStyle: true,
        pointStyle: "rectRounded",
      },
    },
  },
};
